#class & fungsi
#pendeklarasian class menggunakan attribute
class Car:
    def __init__(self): #fungsi
        self.name="" #attribute name
        self.manufacturer="" #attribute manufacturer
        self.year=0
        
car = Car()
# print(car.name)
# print(f"instance object: {car}")
# print(f"type: {type(car)}")
car.name = "M3 GTR"
car.manufacturer = "BMW"
car.year = 2001
print(f"Car name: {car.manufacturer} {car.name}\nYear released:{car.year}\n") #cara mengoutputkan instance attribute
print("Car name: {car.manufacturer} {car.name}\nYear released:{car.year}\n")

#class tanpa attribute
# class Car:
#     def __init_(self):
#         pass
        
        