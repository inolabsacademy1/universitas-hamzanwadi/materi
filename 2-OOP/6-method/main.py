"""
Class method adalah method yang pemiliknya adalah class dengan
pengaksesan adalah via class, berbeda dibanding instance method yang
diperuntukan untuk instance object.
Jika instance method memiliki parameter
self yang isinya adalah instance object, maka class method memiliki
parameter cls yang isinya adalah tipe data class
"""

class ClanHouse:
    def __init__(self,name="",house=""): #instance method
        self.name=name
        self.house=house

    @classmethod
    def create(cls):
        obj=cls()
        return obj
    
    def info(self): #instance method
        print(f"{self.name} of {self.house}")
        
p1=ClanHouse()
p1.name="Tejo Murti"
p1.house="Jogja"
p1.info()

p2 = ClanHouse("Lady Jessica", "Bene Gesserit")
p2.info()

p3 = ClanHouse.create()
p3.name = "Baron Vladimir Harkonnen"
p3.house = "House of Harkonnen"
p3.info()
