"""
penentuan property, baik itu method / attribute dapat diakses
secara public/private
"""
##di Python v2 tidak ada penentuan suatu method/attribute adalah private
##tetapi secara aturan untuk visibilty private dapat ditulis dengan 
#prefix __(2 karakter underscore) 

"""
tetapi pada Python 3, visibility private ditentukan dengan __
"""
# class Product:
#     def __init__(self, name, category):
#         self.name = name
#         self.category = category
#         self.__version = 1.0
    
#     def __print_name(self):
#         print(self.name)

class Company:
    def __init__(self, name = "", products =[]):
        self.name = name
        self.products = products
    
    def __print_name(self):
        print(self.name)
        
    def output_name(self):
        self.__print_name()
        # print(self.name)
        
        # self.name
        # self.__print_name()

# company=Company("Google",["Search Engine","Google Map"])
# company.output_name()

# from Company import __print_name

c1=Company("Google")
# print(f"instance object: {c1}")
# print(f"type: {type(c1.products)}")
print(c1.output_name())
# c1.name="Mic"
# c1.output_name()