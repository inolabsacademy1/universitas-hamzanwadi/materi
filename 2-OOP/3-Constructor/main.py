"""
Konstruktor sendiri adalah fungsi khusus yang dipanggil
saat pembuatan object dilakukan dari suatu class.
"""
class Mountain:
    pass

mount_everest = Mountain() #contoh pemanggilan constructor Mountain()
print(mount_everest)
