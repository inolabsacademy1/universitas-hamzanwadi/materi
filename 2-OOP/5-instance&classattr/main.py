"""Instance Attribute
contoh instance attribute dari class Pencil yaitu note
"""
class Pencil:
    def __init__(self):
        self.note = "A class type to represent a book"
    

pencil1 = Pencil()
print(f"Object pencil1 note: {pencil1.note}")

#Class Attribute
"""Class attribute adalah variabel yang terasosiasi dengan class yang
pengaksesanya bisa langsung dari class atau bisa juga via object (seperti
pengaksesan instance attribute)."""

class Book:
    note = "A class type to represent a book"
    
print(f"Class Book note: {Book.note}")

#Kombinasi Instance Attribute & Class Attribute
class Song:
    note = "A class type to represent a song"
    version = 1.0
    def __init__(self, name = "", artist = "", album = "", released_year= 2000):
        self.name = name
        self.artist = artist
        self.album = album
        self.released_year = released_year
    def info(self):
        print(f"Song: {self.name} by {self.artist}")
        print(f"Album: {self.album}")
        print(f"Released year: {self.released_year}")

songs = [Song(name="The Ytse Jam",artist="Dream Theater",album="When Dream and Day Unite",released_year=2004),Song(name="Romeo",artist="Bon Jovi",album="Galau",released_year=2007)]
# songs[0].info()
for i in songs:
    i.info()
    print()