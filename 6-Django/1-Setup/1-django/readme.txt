#setting env
- setiap django project sebaiknya mempunyai env khusus

#create env
python -m venv myproject

#aktifkan env
source myproject/bin/activate

#install django
python -m pip install django
pip install django_rest_framework

#create project
django-admin startproject todo

#create app
cd todo
django-admin startapp todo_api
python manage.py migrate

#run aplikasi
python manage.py runserver

#create requirement untuk mendaftarkan modules yang diperlukan
pip freeze > requirements.txt

# settings.py
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'todo_api'
]

#creating models for our django app

#After creating the model, migrate it to the database: (setiap melakukan perubahan di fields model, wajib jalankan ini)
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser