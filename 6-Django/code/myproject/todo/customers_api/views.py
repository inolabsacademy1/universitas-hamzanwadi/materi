from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status,permissions
from .models import Customers
from .serializers import CustomersSerializer

class CustomersListApiView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    
    def get(self,request,*args,**kwargs):
        customers=Customers.objects.filter(user=request.user.id)
        
        serializer=CustomersSerializer(customers,many=True)
        return Response(serializer.data,status=status.HTTP_200_OK)
    
    def post(self,request,*args,**kwargs):
        data={
            'full_name':request.data.get('full_name'),
            'email':request.data.get('email'),
            'whatsapp_number':request.data.get('whatsapp_number'),
            'gender':request.data.get('gender'),
            'birth_date':request.data.get('birth_date'),
            'user': request.user.id
        }
        serializer=CustomersSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

class CustomersDetailApiView(APIView):
    # add permission to check if user is authenticated
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, customer_id, user_id):
        '''
        Helper method to get the object with given todo_id, and user_id
        '''
        try:
            return Customers.objects.get(id=customer_id, user = user_id)
        except Customers.DoesNotExist:
            return None

    # 3. Retrieve
    def get(self, request, customer_id, *args, **kwargs):
        '''
        Retrieves the Todo with given todo_id
        '''
        customer_instance = self.get_object(customer_id, request.user.id)
        if not customer_instance:
            return Response(
                {"res": "Object with todo id does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = CustomersSerializer(customer_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # 4. Update
    def put(self, request, customer_id, *args, **kwargs):
        '''
        Updates the todo item with given todo_id if exists
        '''
        todo_instance = self.get_object(customer_id, request.user.id)
        if not todo_instance:
            return Response(
                {"res": "Object with todo id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        data = {
           'full_name':request.data.get('full_name'),
            'email':request.data.get('email'),
            'whatsapp_number':request.data.get('whatsapp_number'),
            'gender':request.data.get('gender'),
            'birth_date':request.data.get('birth_date'),
            'user': request.user.id
        }
        serializer = CustomersSerializer(instance = todo_instance, data=data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # 5. Delete
    def delete(self, request, customer_id, *args, **kwargs):
        '''
        Deletes the todo item with given todo_id if exists
        '''
        customers_instance = self.get_object(customer_id, request.user.id)
        if not customers_instance:
            return Response(
                {"res": "Object with todo id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        customers_instance.delete()
        return Response(
            {"res": "Object deleted!"},
            status=status.HTTP_200_OK
        )    
"""
{
"full_name":"tejo",
"email":"tejo.murti@inolabs.net",
"whatsapp_number":"081234567890",
"gender":"M",
"birth_date":"1989-06-07"
}
"""