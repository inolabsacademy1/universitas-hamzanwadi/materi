from django.urls import path
from .views import (
    CustomersListApiView,
    CustomersDetailApiView
)

urlpatterns=[
    path('api',CustomersListApiView.as_view()),
    path('api/<int:customer_id>/',CustomersDetailApiView.as_view())
]