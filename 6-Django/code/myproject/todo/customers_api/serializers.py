from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from .models import Customers
class CustomersSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[UniqueValidator(queryset=Customers.objects.all())])
    class Meta:
        model=Customers
        fields=["id","full_name","email","whatsapp_number","gender","birth_date","timestamp","user"]