from django.db import models
from django.contrib.auth.models import User

class Customers(models.Model):
    full_name=models.CharField(max_length=255)
    email=models.CharField(max_length=255)
    whatsapp_number=models.IntegerField()
    gender=models.CharField(null=True, max_length=1, blank=True)
    birth_date=models.DateField(null=True, blank=True)
    timestamp=models.DateTimeField(auto_now_add=True,auto_now=False,blank=True)
    user = models.ForeignKey(User, on_delete = models.CASCADE, blank = True, null = True)
    