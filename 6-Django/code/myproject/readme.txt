#Next, add rest_framework and customers to the INSTALLED_APPS inside the restapi/restapi/settings.py file:
# settings.py
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'todo_api'
]

#creating models for our django app

#After creating the model, migrate it to the database:
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser

#create file serializers.py and urls.py

#modify serializers.py

#modify views.py

#modify urls.py

#modify todo/urls.py


#create api views

#Create an endpoint for the class-based view above:
