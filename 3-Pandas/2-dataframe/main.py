"""
DataFrame adalah struktur data tabular dasar yang terdiri dari baris dan kolom.
"""
import pandas as pd
#1-contoh penggunaan list
data=["Garmin","Suntoo","Coros"]
df=pd.DataFrame(data)
print(df)

#2-manipulasi kolom data frame
df=pd.DataFrame(data,columns=["Merk"])
print(df)

#3-menggunakan list
data=[['Ali', 25],['Citra',30],['Budi', 35],]
# print(idx)
df=pd.DataFrame(data,columns=['Nama','Usia'])
print(df)

#4-menggunakan dictionary
data = {'Nama': ['Ali', 'Budi', 'Citra'], 'Usia': [25, 30, 35]}
df = pd.DataFrame(data)
print(df)

#5-modifikasi bagian number
data=[['Ali', 25],['Citra',30],['Budi', 35],]
idx=[i+1 for i in range(len(data))] #menggunakan list comprehension untuk memodifikasi nomor urut
df=pd.DataFrame(data,index=idx,columns=['Nama','Usia'])
print(df)

#6-modifikasi label kolom penomoran
data=[['Ali', 25],['Citra',30],['Budi', 35]]
idx=[i+1 for i in range(len(data))] #menggunakan list comprehension untuk memodifikasi nomor urut
df=pd.DataFrame(data,index=idx,columns=['Nama','Usia'])
df.columns.name = 'No.'
print(df)



"""
KESIMPULAN
1.Salah satu perbedaan utama antara DataFrame dan Seri adalah DataFrame dapat memiliki beberapa kolom, 
sedangkan Seri hanya dapat memiliki satu kolom. Artinya DataFrame dapat menyimpan data yang lebih kompleks dan heterogen, 
sedangkan Seri dapat menyimpan data yang lebih sederhana dan homogen. 

2.Perbedaan lainnya adalah DataFrame dapat memiliki tipe data berbeda untuk setiap kolom, 
sedangkan Seri hanya dapat memiliki satu tipe data untuk keseluruhan array. 
Artinya DataFrame bisa menangani tipe data campuran, seperti angka, string, boolean, atau tanggal, 
sedangkan Seri hanya bisa menangani satu tipe data dalam satu waktu.
"""