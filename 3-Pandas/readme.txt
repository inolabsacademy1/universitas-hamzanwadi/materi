Pandas adalah library python yang bersifat open source 
yang dibuat terutama untuk bekerja dengan data relasional atau berlabel secara mudah. 
Library yang satu ini menyediakan berbagai struktur data dan
operasi untuk memanipulasi data numerik dan deret waktu.

##install di ubuntu
apt-get install python3-pip
apt install python3-venv
python -m venv ~/mypythonenv
source ~/mypythonenv/bin/activate

##example install library
pip install pandas