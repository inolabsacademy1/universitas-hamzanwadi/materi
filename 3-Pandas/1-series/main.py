"""
Pandas adalah library python yang bersifat open source 
yang dibuat terutama untuk bekerja dengan data relasional atau berlabel secara mudah. 
Library yang satu ini menyediakan berbagai struktur data dan
operasi untuk memanipulasi data numerik dan deret waktu.

https://revou.co/panduan-teknis/dataframe-python

#install library
pip install pandas
"""
import pandas as pd

#1-contoh initialisasi
ser=pd.Series()
print(ser)

#contoh penggunaan list
data2=["Garmin","Suntoo","Coros"]
ser=pd.Series(data2)
print(ser)