"""
sample menampilkan dataframe dengan excel

#instal dependency openpyxl 
- pip install openpyxl
"""
import pandas as pd

#menampilkan data dari excel
path="/app/3-Pandas/3-excel/"
# path="/app/3-Pandas/3-excel/sample.xlsx"
df=pd.read_excel(path+"sample.xlsx")
print(df)

#memfilter data
cols = ['NOMOR INVOICE', 'TANGGAL TRANSAKSI', 'TOTAL BELANJA']
df=pd.read_excel(path+"sample.xlsx",sheet_name='Sheet1',usecols=cols)
df = df[df['TOTAL BELANJA'] ==1125000] #menyaring data berdasarkan label
print(df)

#outputkan data yang difilter
cols = ['NOMOR INVOICE', 'TANGGAL TRANSAKSI', 'TOTAL BELANJA']
df=pd.read_excel(path+"sample.xlsx",sheet_name='Sheet1',usecols=cols)
df = df[df['TOTAL BELANJA'] >375000] #menyaring data berdasarkan label
df.to_excel(path+'trx_g375k.xlsx', sheet_name='Transaksi Greater Than 375K') 

print(df)