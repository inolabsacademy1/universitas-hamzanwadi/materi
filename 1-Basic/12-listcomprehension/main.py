"""List comprehension adalah metode ringkas pembuatan list (selain
menggunakan literal [] atau menggunakan fungsi list() )
https://www.w3schools.com/python/python_lists_comprehension.asp
"""

#sample1 perkalian 2
seq = []
for i in range(5):
    seq.append(i * 2)
print(seq)

#sample 1 comprehension
seq = [i * 2 for i in range(5)]
print(seq)

#sample2 menampilkan bilangan ganjil
seq=[]
for i in range(10):
    if i%2==1:
        seq.append(i)
print(seq)        

#sample2 comprehension
seq = [i for i in range(10) if i % 2 == 1]
print(seq)

#sample 3
#jika bilangan habis dibagi dengan 2, maka x 2. jika tidak maka x 3
seq = []
for i in range(1, 10):
    seq.append(i * (2 if i % 2 == 0 else 3))
print(seq)

#sample 3 comprehension
seq=[(i * (2 if i % 2 == 0 else 3)) for i in range(1,10)]
print(seq)
#output [3, 4, 9, 8, 15, 12, 21, 16, 27]

#sample4
list_x=['a','b','c']
list_y=['1','2','3']
seq=[]
for x in list_x:
    for y in list_y:
      seq.append(x+y)  
print(seq)    

#sample 4 comprehension
list_x=['a','b','c']
list_y=['1','2','3']
seq=[x+y for x in list_x for y in list_y]
print(seq)    
#output ['a1', 'a2', 'a3', 'b1', 'b2', 'b3', 'c1', 'c2', 'c3']

#sample5
matrix = [
[1, 2, 3, 4],
[5, 6, 7, 8],
[9, 10, 11, 12],
]
transposed = []
for i in range(4):
    tr = []
    for row in matrix:
        tr.append(row[i])
        # print(row[0])
    transposed.append(tr)
print(transposed)

#sample5 comprehensif
seq=[[row[i] for row in matrix] for i in range(4)]
print(seq)
#output [[1, 5, 9], [2, 6, 10], [3, 7, 11], [4, 8, 12]]