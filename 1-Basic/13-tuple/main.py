"""
Tuple adalah tipe data sequence yang ideal digunakan untuk menampung nilai
kolektif yang isinya tidak akan berubah (immutable)
"""
#sample Tuple
#literal ()
# tuple_1 = (2, 3, 4, "hello python", False)
# print("data:", tuple_1)
# print("total elem:", len(tuple_1))

# ##mengakses element tuple
# tuple_1 = (2, 3, 4, 5)
# print("elem 0:", tuple_1[0])
# # output ➜ elem 0: 2
# print("elem 1:", tuple_1[1])

# #fungsi enumerate untuk menetapkan nilai index dan value
# tuple_2 = ('ultra instinc shaggy', 'nightwing', 'noob saibot')
# for i, v in enumerate(tuple_2):
#     print("index:", i, "elem:", v)
    
#mengecekan apakah element ada dengan menggunakan in
tuple_1 = (10, 70, 20)
n = 70
# for i in tuple_1:
#     if i==n:
#         print(n, "is exists")
        
if n in tuple_1:
    print(n, "is exists")