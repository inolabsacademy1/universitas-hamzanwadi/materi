#tipe data string
#tipe data numeric
#tipe data bool --> True,False


#tipe data list [mutable/bisa diubah]
# list with int as element's data type
list_1 = [2, 4, 8, 16]
# list with str as element's data type
list_2 = ["grayson", "jason", "tim", "damian"]
# list with various data type in the element
list_3 = [24, False, "Hello Python"]

##cara akses list
print(list_1)
print(list_3[2])

#tipe data tuple [imutable / tidak bisa diubah]
# tuple with int as element's data type
tuple_1 = (2, 3, 4)
# tuple with str as element's data type
tuple_2 = ("numenor", "valinor")
# tuple with various data type in the element
tuple_3 = (24, False, "Hello Python")
print(tuple_2[1])


##Tipe Data Dictionary / key value
profile_1 = {
"name": "Tejo Murti",
"is_male": True,
"age": 17,
"hobbies": ["coding","running"]
}
print(profile_1["hobbies"])

##Tipe Data Set / menyimpan data kolektif unik dan tidak ada duplikasi
set_1 = {"pineapple", "spaghetti",'pineapple'}
print(set_1,len(set_1))