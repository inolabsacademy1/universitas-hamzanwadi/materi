"""
Konstanta merupakan nilai yang dideklarasi dari awal
dan tidak bisa diubah.
Menggunakan typing.Final dan perlu diimport.
menggunakan python stdlib
"""

#from untuk menentukan module
#import untuk mengimport sesuatu dari sebuah module
from typing import Final

#deklarasi konstanta dan nilai
PI:Final=3.14
print("Nilai PI: %f"%(PI))


#deklarasi konstanta menggunakan tipe data
API_KEY:Final[str]="1234567"
print("API Key: %s"%(API_KEY))