##Seleksi kondisi Python ➜ if, elif, else
"""
Block indentation
Di python, suatu blok kondisi ditandai dengan indentation atau spasi, yang
menjadikan kode semakin menjorok ke kanan.
"""    

grade = 70
if grade == 100:
    print("perfect")
elif grade == 90:
    print("ok")
    print("keep working hard!")
else:
    print("get out!")

##sebaris | cocok digunakan utk 1 pengkondisian
if grade >= 65: print("passed the exam")
if grade < 65: print("below the passing grade")

#Ternary | cocok digunakan untuk 2 kondisi true, false 
print("passed the exam") if grade >= 65 else print("below the passing grade")

#◉ Ternary dengan nilai balik ditampung pada variable
message = "passed the exam" if grade >= 65 else "below the passing grade"
print(message)