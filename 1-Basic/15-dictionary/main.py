"""
Dictionary merupakan salah satu tipe data mapping di Python
"""

#tipe data ini mempunya key-value
profile = {
"id": 2,
"name": "john wick",
"hobbies": ["playing with pencil"],
"is_female": False,
}

print("data:", profile)
print("total keys:", len(profile))

#opsi lain untuk menampilkan data
import pprint
pprint.pprint(profile)

#opsi ke-2
import json
print(json.dumps(profile,indent=1)) #set nilai indent 1 spasi