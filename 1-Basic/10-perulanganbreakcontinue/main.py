"""
perulangan menggunakan break / continue biasanya digunakan 
untuk memberhentikan perulangan secara paksa ketika suatu kondisi telah terpenuhi
untuk melanjutkan ke iterasi berikutnya
"""

##penggunaan break lebih ke pemberhentian perulangan secara paksa
'''
• Berisi perulangan yang sifatnya berjalan terus-menerus tanpa henti
(karena menggunakan nilai True sebagai kontrol).
• Perulangan hanya berhenti jika nilai n (yang didapat dari inputan user)
adalah tidak bisa dibagi dengan angka 3 .
'''
# while True:
#     n = int(input("enter a number divisible by 3: "))
#     if n % 3 != 0:
#         break
#     print("%d is divisible by 3" % (n))
    

# #penggunaan continue digunakan untuk memaksa agar lanjut ke perulangan berikutnya
# """
# • Program berisi perulangan dengan kontrol adalah data range sebanyak 10
# (dimana isinya adalah angka numerik 0 hingga 9 ).
# • Ketika nilai variabel counter i adalah dibawah 3 atau di atas 7 maka
# iterasi di-skip.
# """
# for i in range(10):
#     if i <= 3 or i >= 7:
#         continue
#     print(i)
    
#label perulangan
'''
• mengontrol perulangan nested dengan konsep label perulangan
• Program yang memiliki perulanga nested dengan jumlah perulangan ada 2.
• Disiapkan sebuah variabel bool bernama outerLoop untuk kontrol
perulangan terluar.
• Ketika nilai j (yang merupakan variabel counter perulangan terdalam)
adalah lebih dari atau sama dengan 7 , maka variabel outerLoop di set
nilainya menjadi False , dan perulangan terdalam di- break secara
paksa.
• Dengan ini maka perulangan terluar akan terhenti.
'''

max = int(input("jumlah bintang: "))
outerLoop = True
for i in range(max):
    if not outerLoop:
        break
    for j in range(i + 1):
        print("*", end=" ")
        if j >= 7: #jika index >=7 maka nilai outerLoop di set false dan perulangan di break
            outerLoop = False
            break
    print()