"""
None merupakan object bawaan Python yang digunakan untuk merepresentasikan data kosong/null
"""
def inspec_data(data):
    if data == None:
        print("data is empty. like very empty")
    else:
        print(f"data: {data}, type: {type(data).__name__}")
        
data=None 
# data="halo" #ketika data berikan 0, data tersebut tidaklah benar2 kosong. 
# tetapi data dengan nilai 0 dan tipe integer
# data=""
inspec_data(data)