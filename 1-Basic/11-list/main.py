# contoh list
list_1 = [10, 70, 20]
# list dengan deklarasi element secara vertikal
list_2 = [
'ab',
'cd',
'hi',
'ca'
]
# list dengan element berisi bermacam-macam tipe data
list_3 = [3.14, 'hello python', True, False]
# list kosong
list_4 = []

#perulangan list
# for i in list_3:
#     print('elem: ',i)

# #perulangan list dengan menggunakan index
#range(param1, param2) range(start,stop)
# for i in range(0,len(list_3)):
#     print(list_3[i],"index:",i)
    
# #menggunakan enumerate. dimana setiap iterasinya bisa diakses index dan elemennya
# list_1 = [10, 70, 20]
for i, v in enumerate(list_3):
    print("index:", i, "elem:", v)
    
# #nested list
# matrix = [
# [0, 1, 0, 1, 0],
# [1, 1, 1, 0, 0],
# [0, 0, 0, 1, 1],
# [0, 1, 1, 1, 0],
# ]
# for row in matrix:
#     for cel in row:
#         print(cel, end=" ")
#     print()