"""
Python Date, Time,
DateTime, Timezone
"""

#menggunakan package datetime
from datetime import date
data_date=date(year=2024,month=6,day=13)
print("date:", data_date)
print(" ➜ year:", data_date.year)
print(" ➜ month:", data_date.month)
print(" ➜ day:", data_date.day)


#menggunakan tipe data class time
from datetime import time
data_time = time(hour=14, minute=00, second=59)
print("time:", data_time)
print(" ➜ hour:", data_time.hour)
print(" ➜ minute:", data_time.minute)
print(" ➜ second:", data_time.second)
print(" ➜ timezone:", data_time.tzinfo)

#Tipe data class datetime untuk penyimpanan informasi tanggal dan waktu
from datetime import datetime
data_datetime = datetime(year=2024, month=6, day=13, hour=13,
minute=14, second=31)
print("datetime:", data_datetime)
print(" ➜ year:", data_datetime.year)
print(" ➜ month:", data_datetime.month)
print(" ➜ day:", data_datetime.day)
print(" ➜ hour:", data_datetime.hour)
print(" ➜ minute:", data_datetime.minute)
print(" ➜ second:", data_datetime.second)
