##Deklarasi Variable
nama="Tejo Murti"
umur=17
tempat_tinggal="Jogja"
is_pria = True

"""Berikut ini adalah cara mengoutputkan variable dengan string formatter"""
print("Hai,")
print("Nama: %s" % nama)
print("Umur: %d" % umur)
print("Tinggal di  %s" % tempat_tinggal)
print("Pria  %r" % is_pria)

##. Deklarasi variabel beserta tipe data
nama : str = "Tejo Murti2"
umur:float=17.5
tempat_tinggal: str ="Jogja"
is_pria : bool= True

print("===Deklarasi variabel beserta tipe data====")
print("Nama: %s" % nama)
print("Umur: %f" % umur)
print("Tinggal di  %s" % tempat_tinggal)
print("Pria  %r" % is_pria)