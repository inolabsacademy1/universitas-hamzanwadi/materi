#operator aritmatika
#operator assignment
#operator perbandingan
#operator logika

"""
operator bitwise
- digunakan untuk pengoperasian dalam bit level
https://www.analyticsvidhya.com/blog/2024/02/bitwise-operators-in-python/
"""

#operator identity (is)
#operator is yang dibandingkan bukan nilai, melainkan identitas
#detail di chapter Object ID & Reference.
num_1 = 100001
num_2 = 100001
res = num_1 is num_2
print("num_1 is num_2 =", res)
print("id(num_1): %s, id(num_2): %s" % (id(num_1), id(num_2)))

# Fungsi id(), mengambil nilai identitas atau ID suatu data
id_num_2 = id(num_2)
print("id_num_2:", id_num_2)

#. Operator membership ( in )
sample_list = [2, 3, 4]
is_3_exists = 3 in sample_list
print(is_3_exists)

sample_tuple = ("hello", "python")
is_hello_exists = "hello" in sample_tuple
print(is_hello_exists)
