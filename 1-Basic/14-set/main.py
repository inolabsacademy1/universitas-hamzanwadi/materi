"""
Set adalah tipe data yang digunakan untuk menampung nilai kolektif unik, jadi tidak ada duplikasi elemen. Elemen
yang ada pada set disimpan secara tidak urut.
Set difungsikan untuk menyelesaikan masalah yang cukup spesifik seperti eliminasi elemen
duplikat.

"""
data_1 = {1, 'abc', False, ('banana', 'spaghetti')}
print("data:", data_1)
# output ➜ data: {1, 'abc', False, ('banana', 'spaghetti')}
print("len:", len(data_1))

#mengakses element set
fellowship = {'aragorn', 'gimli', 'legolas'}
for p in fellowship:
    print(p)
    
#mengecek apakah element x exist
to_find = 'gimli'
if to_find in fellowship:
    print("exist")