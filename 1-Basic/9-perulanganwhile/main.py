#perulangan while dikontrol dengan operasi logika atau nilai boolean
# should_continue=True
# while should_continue:
#     n=int(input("silakan input angka genap dan lebih besar dari 0: "))
#     if n<=0 or n%2==1:
#         print(n," adalah tidak lebih besar dari 0 atau bukan angka genap")
#     else:
#         print("number: ",n)


#perulangan dengan menginputkan nilai dan penggunaan increament
# n = int(input("enter max data: "))
# i = 0
# while i < n:
#     print("number", i)
#     i +=1 #penggunaan increament
    
    
# #perulangan dengan menginputkan nilai dan penggunaan decreament
n = int(input("enter max data: "))
i = 0
while n > i:
    print("number", n)
    n -=1 #penggunaan decreament
    

# #perulangan bercabang / nested
# n = int(input("enter max data: "))
# i = 0
# while i < n:
#     j = 0
#     while j < n - i:
#         print("*", end=" ") #menggunakan parameter end opsional
#         j += 1
#     print()
#     i += 1