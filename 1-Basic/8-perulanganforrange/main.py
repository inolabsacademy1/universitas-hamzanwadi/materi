#looping 
#nilai awal dimulai dari 0 dan di looping sebanyak 5 kali
# range(n), menghasilkan data range sejumlah n

for i in range(5):
    print("index:", i)
    
#List
#menampung perulangan di dalam List
# list_biasa=[0,1,2,3,4]
r = range(5)
print("r:", list(r))


""" Alternatif penggunaan range"""
#pengkondisian range start dan stop. range(start,stop)
for i in range(1, 3): #0,1,2
     print("ss-index:", i)
     
#pengkondisian range start, stop, step. range(start,stop,step)
for i in range(1, 7, 2): #0,1,2,3,4,5,6
     print("sss-index:", i)
     
## Iterasi data list
messages = ["morning", "afternoon", "evening"]
for m in messages:
    print(m)

#Iterasi data tuple
numbers = ("twenty four", 24)
for n in numbers:
    print(n)

#Iterasi data string
for char in "hello inolabs":
    print(char)
    
# Iterasi data dictionary
bio = {
"name": "Inolabs Academy x Universitas Hamzanwadi",
"year": 2024,
}
for key in bio:
    print("key:", key, "value:", bio[key])
    
# Iterasi data set
numbers = {"twenty four", 24}
for n in numbers:
    print(n)
    
#Perulangan bercabang / nested for
#menggunakan opsional end
#jika tanpa opsional end, defaultnya adalah \n (menghasilkan baris baru)
# max = int(input("jumlah bintang: "))
max = 5
for i in range(max):
    for j in range(0, max - i):
        print("*", end=" ") #fungsi print dengan parameter opsional end
    print() #fungsi print tanpa parameter outputnya adalah baris baru