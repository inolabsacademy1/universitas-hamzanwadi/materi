
import mysql.connector

mydb = mysql.connector.connect(
  host="server-mariadb-10",
  user="root",
  password="root",
  database="ia_python"
)

mycursor = mydb.cursor()

# #insert single row
# sql = "INSERT INTO customers (full_name, age, total_bill) VALUES (%s, %s, %s)"
# val = ("John Wick", 30, 0)
# mycursor.execute(sql, val)

# mydb.commit()
# #mydb.commit(). It is required to make the changes, otherwise no changes are made to the table.

# print(mycursor.rowcount, "record inserted.")


#insert multiple row
sql = "INSERT INTO customers (full_name, age, total_bill) VALUES (%s, %s, %s)"
val = [
  ('Dian', 40,2000),
  ('Amy', 18,2500000),
  ('Hannah', 19,50000),
  ('Michael', 20,400000),
  ('Sandy', 21,1000000),
  ('Viola', 22,250000)
]
mycursor.executemany(sql, val) #menggunakan executemany

mydb.commit()
#mydb.commit(). It is required to make the changes, otherwise no changes are made to the table.

print(mycursor.rowcount, "record inserted.")
print(mycursor.lastrowid , "lastrowid .")