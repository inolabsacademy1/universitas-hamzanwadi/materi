"""
koneksi python dengan MySQL 
- install connector
"""

#install
#pip install mysql-connector-python

#import koneksi
import mysql.connector

mydb = mysql.connector.connect(
  host="server-mariadb-10",
  user="root",
  password="root"
)

print(mydb)

"""
CREATE Database

CREATE TABLE `ia_python`.`customers` (`id` INT NOT NULL AUTO_INCREMENT , `full_name` VARCHAR(255) NOT NULL , `age` INT NOT NULL , `total_bill` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
"""