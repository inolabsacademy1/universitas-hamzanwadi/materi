
import mysql.connector

mydb = mysql.connector.connect(
  host="server-mariadb-10",
  user="root",
  password="root",
  database="ia_python"
)

mycursor = mydb.cursor()
#delete
sql = "DELETE FROM customers WHERE id=1"
mycursor.execute(sql) 
mydb.commit() #It is required to make the changes, otherwise no changes are made to the table.

print(mycursor.rowcount, "record deleted.")

# #prevent sql injection
# sql = "DELETE FROM customers WHERE id = %s"
# adr = (3, )
# mycursor.execute(sql, adr)
# mydb.commit()
# print(mycursor.rowcount, "record(s) deleted")