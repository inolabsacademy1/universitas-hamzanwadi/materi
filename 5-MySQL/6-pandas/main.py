import mysql.connector
import pandas as pd

mydb = mysql.connector.connect(
  host="server-mariadb-10",
  user="root",
  password="root",
  database="ia_python"
)

mycursor = mydb.cursor()

#mendapatkan semua data
mycursor.execute("SELECT * FROM customers")
myresult = mycursor.fetchall()
print("fetchAll: ",myresult)
df=pd.DataFrame(myresult,columns=['Id','Nama','Umur','Belanja'])
print(df)