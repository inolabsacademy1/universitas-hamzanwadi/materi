
import mysql.connector

mydb = mysql.connector.connect(
  host="server-mariadb-10",
  user="root",
  password="root",
  database="ia_python"
)

mycursor = mydb.cursor()
#update
# sql = "UPDATE customers SET full_name='Mishary' WHERE id=2"
# mycursor.execute(sql) 
# mydb.commit() #It is required to make the changes, otherwise no changes are made to the table.

# print(mycursor.rowcount, "record updated.")

# #prevent sql injection
sql = "UPDATE customers SET full_name=%s,age=%s WHERE id = %s"
adr = ('Mishary Mufid',2,2)
mycursor.execute(sql, adr)
mydb.commit()
print(mycursor.rowcount, "record(s) updated")