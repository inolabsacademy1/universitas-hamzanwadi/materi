import mysql.connector

mydb = mysql.connector.connect(
  host="server-mariadb-10",
  user="root",
  password="root",
  database="ia_python"
)

mycursor = mydb.cursor()

#mendapatkan semua data
mycursor.execute("SELECT * FROM customers")
myresult = mycursor.fetchall()
print("fetchAll: ",myresult)
# for x in myresult:
#   print(x)
  
#mendapatkan 1 data saja
mycursor.execute("SELECT * FROM customers")
myresult = mycursor.fetchone()
print("fetchOne: ",myresult)

# for x in myresult:
#   print(x)