
import pandas as pd
import numpy as np
#series
data=np.array(["Tejo","Dian","Vino","Saka"])
ser=pd.Series(data)
print(ser)

#data frame
data=np.array([["Tejo",17],["Dian",40],["Ria Ricis",25],["Inolabs",None]])
df=pd.DataFrame(data,columns=['Nama','Umur'])
print(df)