"""
https://revou.co/panduan-teknis/numpy-python
Array NumPy dianggap lebih efisien daripada list Python biasa, mengapa demikian?
Kecepatan: array NumPy dirancang untuk operasi matematika dan ilmiah. Berkat optimasi yang dilakukan di tingkat sistem atau internal NumPy, operasi pada array NumPy berjalan lebih cepat dibandingkan list Python biasa.
Fleksibilitas: kamu dapat melakukan operasi pada seluruh array sekaligus tanpa perlu loop. Misalnya, untuk mengalikan setiap elemen dalam array dengan angka tertentu, kamu hanya memerlukan satu baris kode.
Penggunaan memori: array NumPy memerlukan memori yang lebih sedikit dibandingkan dengan list. Hal ini dikarenakan NumPy dirancang khusus untuk efisiensi memori, terutama saat berurusan dengan data dalam jumlah besar.
Fungsionalitas tambahan: Selain operasi dasar, NumPy juga menyediakan berbagai fungsi matematika dan statistika siap pakai yang tidak tersedia di list Python biasa.
"""
import numpy as np

#contoh penggunaan array pada list biasa
list_biasa=[1,2,3,4,5,6]
print("list: ",list_biasa)

#contoh penggunaan di numpy
list_np=np.array([1,2,3,4,5,6])
print("np:",list_np)

#operasi aritmatika
list_np2=np.array([1,2,3])
list_np3=np.array([4,5,6])

hasil=list_np2+list_np3
print("hasilAritmatika:",hasil)

#pengunaan untuk fungsi statistika
umur_tim_inolabs=np.array([18,19,19,20,25,30,28])
rata_rata = np.mean(umur_tim_inolabs)
print("rata_rata: umur_tim_inolabs:",rata_rata)
median = np.median(umur_tim_inolabs)
print("median: umur_tim_inolabs:",median)

